# FileValidate

### 介绍
在.Net中，寻找比较2个字节数组是否相同的最快方法（尤其是超大文件）！

###  编程环境：
vs2019/.net core 3.0

###  比较方式：
1. 文件大小不一样，直接返回 False
2. MD5校验
3. 字节数组每一个字节校验
4. ReadOnlySpan<byte> 校验  注：.net core 3.0
5. DllImport C库函数 memcmp  校验

### 使用说明
```csharp
FileValidate fileValidate = new FileValidate(srcFile, tgtFile, valiteType: 4);
fileValidate.IsSame()
```

### 测试截图
#### 单次测试
![1次测试](https://gitee.com/lztkdr/FileValidate/raw/master/Test_Images/Test_Result.png)

#### 5次测试
![5次测试](https://gitee.com/lztkdr/FileValidate/raw/master/Test_Images/Test_Result_5.png)

#### 10次测试
![10次测试](https://gitee.com/lztkdr/FileValidate/raw/master/Test_Images/Test_Result_10.png)

####50次测试
![50次测试](https://gitee.com/lztkdr/FileValidate/raw/master/Test_Images/Test_Result_50.png)

### 结论
```
	memcmp ≈ ReadOnlySpan > ByteCompare > MD5Compare
```
- MD5检验效率最低，无可厚非，源于又要哈希成byte字节数组，最后又要转成字符串。
- Byte数组的每个字节去比较，很普通，没用什么算法的比较。
- ReadOnlySpan .net core 3.0 中提供的内存安全高效访问的BCL,大量API的性能提升使用该结构体。
- memcmp C函数库方法，很多文章都介绍到该库函数的卓越的执行效率。
- 随着测试次数的增加，竟然发现 ReadOnlySpan 并不比 memcmp 差多少，甚至已经超越 ！
- 微软大法，不错哈！C#是世界上最好的语言！哈哈哈！

### 参与贡献
- 欢迎参与评论，拉去代码进行测试，或加入新检验方法,让我们思维的火花继续碰撞，在相对好的方法上，寻找最佳解决方法，精益求精的极客精神！

### 参考链接
- [.NET CORE下最快比较两个文件内容是否相同的方法](https://www.cnblogs.com/waku/p/11069214.html)
- [C#用memcmp比较字节数组](https://www.cnblogs.com/zgqys1980/archive/2009/07/13/1522546.html)

