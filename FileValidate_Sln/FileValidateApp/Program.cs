﻿using ES.Transfer;
using System;
using System.Diagnostics;
using System.IO;
using System.Numerics;

namespace BytesCompareApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var srcFile = @"E:\tmp\001.MXF";//大文件，超过25.4G

            var tgtFile = @"E:\001.MXF";

            FileInfo info = new FileInfo(srcFile);

            Console.WriteLine("文件大小：" + CountSize(info.Length));

            //执行次数
            long exeCount = 50;
            Console.WriteLine("每个方式执行：" + exeCount + "次");

            Console.WriteLine("MD5方式：" + new Action(() =>
            {
                FileValidate fileValidate = new FileValidate(srcFile, tgtFile, valiteType: 1);
                var res = fileValidate.IsSame();
                //Console.WriteLine(res + "\t总检验次数：" + fileValidate.TotalValidateCount);

            }).ForWatchSec(exeCount));


            Console.WriteLine("Byte方式：" + new Action(() =>
            {
                FileValidate fileValidate = new FileValidate(srcFile, tgtFile, valiteType: 2);
                var res = fileValidate.IsSame();
                //Console.WriteLine(res + "\t总检验次数：" + fileValidate.TotalValidateCount);

            }).ForWatchSec(exeCount));

            Console.WriteLine("ReadOnlySpan方式：" + new Action(() =>
            {
                FileValidate fileValidate = new FileValidate(srcFile, tgtFile, valiteType: 3);
                var res = fileValidate.IsSame();
                //Console.WriteLine(res + "\t总检验次数：" + fileValidate.TotalValidateCount);

            }).ForWatchSec(exeCount));

            Console.WriteLine("Memcmp方式：" + new Action(() =>
            {
                FileValidate fileValidate = new FileValidate(srcFile, tgtFile, valiteType: 4);
                var res = fileValidate.IsSame();
                //Console.WriteLine(res + "\t总检验次数：" + fileValidate.TotalValidateCount);

            }).ForWatchSec(exeCount));


            // memcmp ≈ ReadOnlySpan > ByteCompare > md5


            Console.ReadKey();
        }

        /// <summary>
        /// 计算文件大小函数(保留两位小数),Size为字节大小
        /// </summary>
        /// <param name="Size">初始文件大小</param>
        /// <returns></returns>
        public static string CountSize(long Size)
        {
            string m_strSize = "";
            long FactSize = 0;
            FactSize = Size;
            if (FactSize < 1024.00)
                m_strSize = FactSize.ToString("F2") + " Byte";
            else if (FactSize >= 1024.00 && FactSize < 1048576)
                m_strSize = (FactSize / 1024.00).ToString("F2") + " K";
            else if (FactSize >= 1048576 && FactSize < 1073741824)
                m_strSize = (FactSize / 1024.00 / 1024.00).ToString("F2") + " M";
            else if (FactSize >= 1073741824)
                m_strSize = (FactSize / 1024.00 / 1024.00 / 1024.00).ToString("F2") + " G";
            return m_strSize;
        }

    }
}
